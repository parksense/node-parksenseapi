var mongoose = require('mongoose');
var User = require('./user');
var Sensor = require('./sensor');

var ParkSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  type: {
    type: String,
    enum: ['Gated', 'Side Road', 'Other'],
    default: 'Gated'
  },
  size: {
    type: Number,
    default: 0  
  },
  alerts: [{
    type: String  
  }],
  description: {
    type: String  
  },
  notes: {
    type: String  
  },
  photo: {
    data: Buffer, 
    contentType: String
  },
  geofence: {
    type: Number,
    default: 0
  },
  loc: {
    type: [Number],  // [<longitude>, <latitude>]
    index: '2d'      // create the geospatial index
  },
  occupation: {
    type: Number,
    default: 0 
  },
  schedule: { 
    open: String, 
    close: String 
  },
  tariff: [{ 
    time: Number, 
    price: Number 
  }],
  _owner: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User' 
  },
  offers: [{ 
    description: String
  }],
  _sensors: [{ 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Sensor' 
  }],
  available: {
    type: Boolean
  },
  enabled: {
    type: Boolean
  },
  createdAt: { 
    type: Date, 
    default: Date.now 
  }
});

module.exports = mongoose.model('Park', ParkSchema);