var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var ParkingLog = require('./parkingLog');
var Park = require('./park');

// Schema defines how the user data will be stored in MongoDB
var UserSchema = new mongoose.Schema({
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  points: {
    type: Number,
    default: 0
  },
  createdAt: { 
    type: Date, 
    default: Date.now 
  },
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  role: {
    type: String,
    enum: ['User', 'Manager', 'Admin'],
    default: 'User'
  },
  _parkingLogs: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ParkingLog' }],
  _favouriteParks: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Park' }]
});

// Saves the user's password hashed (plain text password storage is not good)
UserSchema.pre('save', function (next) {
  var user = this;
  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) {
          return next(err);
        }
        user.password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});

// Create method to compare password input to password saved in database
UserSchema.methods.comparePassword = function(pw, cb) {
  bcrypt.compare(pw, this.password, function(err, isMatch) {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

// Create method to create a reset password token
UserSchema.methods.resetPassword = function(user,token,cb) {
  user.resetPasswordToken = token;
  user.resetPasswordExpires = Date.now() + 43200000; // now + 12h
  user.save(function(err) {
    console.log(err)
    if (err) {
      cb(err);
    }
    cb(null);
  });
};

// Create method to set a new password
UserSchema.methods.newPassword = function(user,pw,cb) {
  user.password = pw;
  user.resetPasswordToken = null;
  user.resetPasswordExpires = null;
  user.save(function(err) {
    console.log(err)
    if (err) {
      cb(err);
    }
    cb(null);
  });
};

module.exports = mongoose.model('User', UserSchema);