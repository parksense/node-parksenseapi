var mongoose = require('mongoose');
var SensorLog = require('./sensorLog');

var SensorSchema = new mongoose.Schema({
  loc: {
    type: [Number],  // [<longitude>, <latitude>]
    index: '2d'      // create the geospatial index
  },
  createdAt: { 
    type: Date, 
    default: Date.now 
  },
  notes: {
    type: String
  },
  alerts: [{
    type: String
  }],
  type: {
    type: String,
    enum: ['Directional', 'Occupation', 'Other'],
    default: 'Directional'
  },
  state: {
    type: String
  },
  _logs: [{ type: mongoose.Schema.Types.ObjectId, ref: 'SensorLog' }]
});

module.exports = mongoose.model('Sensor', SensorSchema);