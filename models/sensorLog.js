var mongoose = require('mongoose');

var sensorLogSchema = new mongoose.Schema({
  date: { 
    type: Date, 
    default: Date.now 
  },
  loc: {
    type: [Number],  // [<longitude>, <latitude>]
    index: '2d'      // create the geospatial index
  },
  count: {
    type: Number  
  }
});

module.exports = mongoose.model('SensorLog', sensorLogSchema);