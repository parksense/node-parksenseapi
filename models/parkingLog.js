var mongoose = require('mongoose');
var Park = require('./park');

var parkingLogSchema = new mongoose.Schema({
  date: { 
    type: Date, 
    default: Date.now 
  },
  loc: {
    type: [Number],  // [<longitude>, <latitude>]
    index: '2d'      // create the geospatial index
  },
  parkName: {
    type: String  
  },
  _park: { type: mongoose.Schema.Types.ObjectId, ref: 'Park' }
});

module.exports = mongoose.model('ParkingLog', parkingLogSchema);