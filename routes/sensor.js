var express = require('express');
var router = express.Router();
var Sensor = require('./../models/sensor');
var SensorLog = require('./../models/sensorLog');
var Park = require('./../models/park');
var mongoose = require('mongoose');
var passport = require('passport');
var jwt = require('jsonwebtoken');

// Get a sensor
router.get('/:id', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.params.id) {
    return next(err);
  } else {
    Sensor.findById(req.params.id, function(err, sensor) {
      if (err) {
        return next(err);
      } else {
        res.json({ 
          success: true, 
          message: 'OK', 
          cod: 200,
          item: {
            loc: sensor.loc,
            notes: sensor.notes,
            alerts: sensor.alerts,
            type: sensor.type,
            state: sensor.state,
            logs: sensor._logs
          }
        });
      } 
    });
  }
});

// Add a new sensor
router.post('/', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.body.lat || !req.body.lon) {
    return next(err);
  } else {
    var loc =  [req.body.lon, req.body.lat];
    var newSensor = new Sensor({
      type: req.body.type,
      loc: loc,
    });
    newSensor.save(function(err) {
      if (err) {
        return next(err);
      } else {
        res.json({ success: true, message: 'OK', cod: 200 });
      } 
    });
  }
});

// Update a sensor
router.patch('/:id', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.params.id) {
    return next(err);
  } else {
    Sensor.findById(req.params.id, function(err, sensor) {
      if (err) {
        return next(err);
      } else {
        var loc =  [req.body.lon, req.body.lat];
        sensor.loc = loc,
        sensor.type = req.body.type
        sensor.save(function(err) {
          if (err){
            return next(err);
          } else {
            res.json({ success: true, message: 'OK', cod: 200 });
          }
        });
      } 
    });
  }
});

// Remove a sensor
router.delete('/', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.body.id) {
    return next(err);
  } else {
    Sensor.findByIdAndRemove(req.body.id, function(err) {
      if (err) {
        return next(err);
      } else {
        res.json({ success: true, message: 'OK', cod: 200 });
      } 
    });
  }
});

// Add a new occupation log to a sensor
router.put('/occupation', function(req, res, next) {
  if(!req.query.value || !req.query.id) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Sensor.findById(req.query.id, function(err, sensor) {
      if (err){
         return next(err);
      } else {
        var newSensorLog = new SensorLog({
          count: req.query.value,
          loc: sensor.loc
        });
        newSensorLog.save(function(err) { 
          if (err){
            return next(err);
          } 
        });
        sensor._logs.push(newSensorLog._id);
        sensor.save(function(err) {
          if (err){
            return next(err);
          } 
        });
        Park.update({ 
          '_sensors': mongoose.Types.ObjectId(req.query.id)
        },{
          $inc: { 
            'occupation': req.query.value 
          }
        }, function(err, park) {
          //console.log(park)
        });
        res.json({ success: true, message: 'OK', cod: 200 });
      }
    }); 
  }
});

// Add a new message to a sensor
router.put('/message', function(req, res, next) {
  if(!req.query.value || !req.query.id) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Sensor.findById(req.query.id, function(err, sensor) {
      if (err){
         return next(err);
      } else {
        sensor.alerts.push(req.query.value);
        sensor.save(function(err) {
          if (err){
            return next(err);
          } 
        });
        res.json({ success: true, message: 'OK', cod: 200 });
      }
    }); 
  }
});


module.exports = router;
