var express = require('express');
var router = express.Router();
var passport = require('passport');
var jwt = require('jsonwebtoken');
var User = require('./../models/user');
var ParkingLog = require('./../models/parkingLog');
var config = require('./../config/main');
var emails = require('./../config/emails');
var mongoose = require('mongoose');
var async = require('async');
var Park = require('./../models/park');

/*
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
*/

// Get parking logs
router.post('/log/history', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.userId) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    User.findById(req.body.userId, function(err, user) {
      if (err) {
        return next(err);
      } else {
        var array = user._parkingLogs;
        async.map(array, function (key, next) {
          ParkingLog.findById(key, function (err, log) {
            if (err){
              throw err;
            } else {
              if(log){
                next(err, log);
              }
            }
          });
        },
        function (err, result) {
          var o = steamrollArray(result);
          res.json({ success: true, message: 'OK', cod: 200, items: o });
        });
      } 
    }); 
  }
});

// Get favourite parks
router.get('/favourite/:id', function(req, res, next) {
  if(!req.params.id) {
    return next(err);
  } else {
    User.findById(req.params.id, function(err, user) {
      if (err) {
        return next(err);
      } else {
        var array = user._favouriteParks;
        async.map(array, function (key, next) {
          Park.findById(key, function (err, park) {
            if (err){
              throw err;
            } else {
              if(park){
                next(err, park);
              }
            }
          });
        },
        function (err, result) {
          var o = steamrollArray(result);
          res.json({ success: true, message: 'OK', cod: 200, items: o });
        });
      } 
    }); 
  }
});

// Remove a park as a favourite
router.delete('/favourite', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.parkId) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    User.update(
      { 
        _id: req.user._id
      },
      { 
        $pull: { '_favouriteParks': req.body.parkId }
      }, function(err, user) {
      if (err){
         return next(err);
      } else {
        res.json({ success: true, message: 'OK', cod: 200 });
      }
    });   
  }
});

// Add a park as a favourite
router.post('/favourite', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.parkId) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    User.update({ 
      '_id': req.user._id
    },{
      $push: { 
        '_favouriteParks': mongoose.Types.ObjectId(req.body.parkId)
      }
    }, function(err, user) {
      if (err){
        return next(err);
      } 
      res.json({ success: true, message: 'OK', cod: 200 });
    });    
  }
});

// Add a parking log to a user
router.post('/log', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.lat || !req.body.lon || !req.body.parkId) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    User.findById(req.user._id, function(err, user) {
      if (err){
         return next(err);
      } else {
        console.log(user)
        Park.findById(req.body.parkId, function(err, park) {
          console.log(park)
          var loc =  [req.body.lon, req.body.lat];
          var newParkingLog = new ParkingLog({
            loc: loc,
            _park: mongoose.Types.ObjectId(req.body.parkId),
            parkName: park.name
          });
          newParkingLog.save(function(err) { 
            if (err){
              return next(err);
            } 
          });
          user._parkingLogs.push(newParkingLog._id);
          user.save(function(err) {
            if (err){
              return next(err);
            } 
          });
          res.json({ success: true, message: 'OK', cod: 200 });
        });
      }
    }); 
  }
});

// Register new admin
router.post('/registerAdmin', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.body.email || !req.body.password || !req.body.role) {
    res.json({ success: false, message: 'Please enter email and password.', cod: 400 });
  } else {
    if (req.user.role != config.adminRole) {
      res.json({ success: false, message: 'Unauthorized.', cod: 401 });
    } else {
      var newUser = new User({
        role: req.body.role,
        email: req.body.email,
        password: req.body.password
      });

      newUser.save(function(err, user) {
        if (err) {
          throw err;
          //return res.json({ success: false, message: 'That email address already exists.', cod: 400});
        }
        var token = jwt.sign(user, config.secret, {
            expiresIn: config.tokenexpire 
        });
        res.json({ success: true, token: token, message: 'OK', cod: 200 });
        emails.registerEmail(req.body.email, req.body.password);
      });
    }
  }
});

// Register new users
router.post('/register', function(req, res) {
  if(!req.body.email || !req.body.password) {
    res.json({ success: false, message: 'Please enter email and password.', cod: 400 });
  } else {
    var newUser = new User({
      email: req.body.email,
      password: req.body.password
    });

    newUser.save(function(err, user) {
      if (err) {
        throw err;
        //return res.json({ success: false, message: 'That email address already exists.', cod: 400});
      }
      var token = jwt.sign(user, config.secret, {
          expiresIn: config.tokenexpire 
      });
      res.json({ success: true, token: token, message: 'OK', cod: 200 });
      emails.registerEmail(req.body.email, req.body.password);
    });
  }
});

// Authenticate the user and get a JSON Web Token to include in the header of future requests.
router.post('/authenticate', function(req, res) {
  User.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.send({ success: false, message: 'Authentication failed. User not found.' });
    } else {
      user.comparePassword(req.body.password, function(err, isMatch) {
        if (isMatch && !err) {
          var token = jwt.sign(user, config.secret, {
            expiresIn: config.tokenexpire
          });
          res.json({ success: true, token: 'JWT ' + token, message: 'OK', cod: 200 });
        } else {
          res.send({ success: false, message: 'Authentication failed. Passwords did not match.', cod: 400 });
        }
      });
    }
  });
});

// Resets the password assigning a temporary token.
router.post('/forgot', function(req, res) {
  User.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.send({ success: false, message: 'Reset failed. User not found.', cod: 400 });
    } else {
      var token = jwt.sign(user, config.resetsecret);
      user.resetPassword(user,token,function(err) {
        if (err) {
          return res.json({ success: false, message: 'Error.'});
        }
        res.json({ success: true, token: token, message: 'OK', cod: 200 });
        emails.passwordResetEmail(req.body.email, token);
      });
    }
  });
});

// Resets the password to a new one
router.post('/reset/:token', function(req, res) {
  if(!req.body.password) {
    res.json({ success: false, message: 'Please enter password.', cod: 400 });
  } else {
    User.findOne({ 
      resetPasswordToken: req.params.token, 
      resetPasswordExpires: { $gt: Date.now() } 
    }, function(err, user) {
      if (err) throw err;

      if (!user) {
        res.send({ success: false, message: 'Password reset token is invalid or has expired.', cod: 400 });
      } else {
        user.newPassword(user,req.body.password,function(err) {
          if (err) {
            return res.json({ success: false, message: 'Error.'});
          }
          res.json({ success: true, message: 'OK', cod: 200 });
        });
      }
    });
  }
});

// Test the authentication returning the token
router.get('/testauth', passport.authenticate('jwt', { session: false }), function(req, res) {
  res.send('It worked! User id is: ' + req.user._id + '.' + req.user.email);
});

// Flatten a nested array
function steamrollArray(arr) {
  
  var o = [];
  
  for(var j=0;j<arr.length;j++){
    if(Array.isArray(arr[j])){
      subArray(arr[j]);
    } else {
      o.push(arr[j]);
    }
    
  }
  
  function subArray(subarr){
    for(var i=0;i<subarr.length;i++){
      if(Array.isArray(subarr[i])){
        subArray(subarr[i]);
      } else {
        o.push(subarr[i]);
      }
    }
  }
  
  return o;
}

module.exports = router;
