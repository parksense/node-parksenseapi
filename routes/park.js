var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var moment = require('moment');
var async = require('async');
var Sensor = require('./../models/sensor');
var SensorLog = require('./../models/sensorLog');
var User = require('./../models/user');
var config = require('./../config/main');
var Park = require('./../models/park');

// Get all parks belonging to an owner 
router.post('/ownerparks', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  Park.find({
    _owner: req.user._id
  }, function(err, parks) {
    console.log(req.user._id)
    if (err){
        return next(err);
    } else {
      res.json({ success: true, message: 'OK', cod: 200, items: parks });
    }
  }); 
});

// Get all parks within a geo box belonging to a manager
router.post('/ownerparksbox', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.bllon, !req.body.bllat, !req.body.urlat, !req.body.urlon) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.find({
      loc: { $geoWithin: { $box:  [ [ req.body.bllon, req.body.bllat ], [ req.body.urlon, req.body.urlat ] ] } },
      _owner: req.user._id
    }, function(err, parks) {
      if (err){
         return next(err);
      } else {
        res.json({ success: true, message: 'OK', cod: 200, items: parks });
      }
    }); 
  }
});

// Get all parks within a geo box
router.post('/parksbox', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.bllon, !req.body.bllat, !req.body.urlat, !req.body.urlon) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.find({
      loc: { $geoWithin: { $box:  [ [ req.body.bllon, req.body.bllat ], [ req.body.urlon, req.body.urlat ] ] } }
    }, function(err, parks) {
      if (err){
         return next(err);
      } else {
        res.json({ success: true, message: 'OK', cod: 200, items: parks });
      }
    }); 
  }
});

// Get all sensors from a park
router.get('/sensors/all/:id', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.params.id) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.findById(req.params.id, function(err, park) {
      var array = park._sensors;
      async.map(array, function (key, next) {
        Sensor.findById(key, function (err, sensor) {
          if (err){
            throw err;
          } else {
            if(sensor){
              next(err, sensor);
            }
          }
        });
      },
      function (err, result) {
        var o = steamrollArray(result);
        res.json({ success: true, message: 'OK', cod: 200, items: o });
      });
    }); 
  }
});

// Get the last 20 logs
router.post('/logs/latest', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.id) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.findById(req.body.id, function(err, park) {
      var array = park._sensors;
      async.map(array, function (key, next) {
        Sensor.findById(key, function (err, sensor) {
          if (err){
            throw err;
          } else {
            if(sensor._logs){
              next(err, sensor._logs);
            }
          }
        });
      },
      function (err, result) {
        var flattenedArray = steamrollArray(result);
        async.map(flattenedArray, function (key, next) {
          SensorLog.find({ 
            _id: key
          }, function (err, log) {
            if (err){
              throw err;
            } else {
              next(err, log);
            }
          });
        },
        function (err, result) {
          var o = steamrollArray(result);
          res.json({ success: true, message: 'OK', cod: 200, items: o });
        });
      });
    }); 
  }
});

// Get logs from a range of dates
router.post('/logs/range', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.id || !req.body.maxDate || !req.body.minDate) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.findById(req.body.id, function(err, park) {
      var array = park._sensors;
      
      async.map(array, function (key, next) {
        Sensor.findById(key, function (err, sensor) {
          if (err){
            throw err;
          } else {
            if(sensor._logs){
              next(err, sensor._logs);
            }
          }
        });
      },
      function (err, result) {
        var flattenedArray = steamrollArray(result);
        //var gtdate = moment(req.body.gtyear + '-' + req.body.gtmonth + '-' +req.body.gtday, "YYYY-M-D");
        //var ltdate = moment(req.body.ltyear + '-' + req.body.ltmonth + '-' +req.body.ltday, "YYYY-M-D");
        var gtdate = moment(req.body.minDate).format('YYYY-MM-DD');
        var ltdate = moment(req.body.maxDate).add(1, 'days').format('YYYY-MM-DD');
        async.map(flattenedArray, function (key, next) {
          SensorLog.find({ 
            _id: key, 
            date: {
              $gte: gtdate,
              $lte: ltdate
            } 
          }, function (err, log) {
            if (err){
              throw err;
            } else {
              //console.log(log)
              next(err, log);
            }
          });
        },
        function (err, result) {
          var o = steamrollArray(result);
          res.json({ success: true, message: 'OK', cod: 200, items: o });
        });
      });
    }); 
  }
});

// Get logs from a single day
router.post('/logs/single', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.id || !req.body.day || !req.body.month || !req.body.year) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.findById(req.body.id, function(err, park) {
      var array = park._sensors;
      async.map(array, function (key, next) {
        Sensor.findById(key, function (err, sensor) {
          if (err){
            throw err;
          } else {
            if(sensor._logs){
              next(err, sensor._logs);
            }
          }
        });
      },
      function (err, result) {
        var flattenedArray = steamrollArray(result);
        var today = moment(req.body.year + '-' + req.body.month + '-' +req.body.day, "YYYY-M-D");
        var tomorrow = moment(today).add(1, 'days');
        async.map(flattenedArray, function (key, next) {
          SensorLog.find({ 
            _id: key, 
            date: {
              $gte: today,
              $lt: tomorrow
            } 
          }, function (err, log) {
            if (err){
              throw err;
            } else {
              next(err, log);
            }
          });
        },
        function (err, result) {
          var o = steamrollArray(result);
          res.json({ success: true, message: 'OK', cod: 200, items: o });
        });
      });
    }); 
  }
});

// Remove a tariff from a park
router.delete('/tariff/:id', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.tariffId || !req.params.id) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.update(
      { 
        _id: req.params.id
      },
      { 
        $pull: { 'tariff': { '_id': req.body.tariffId } } 
      }, function(err, park) {
      if (err){
         return next(err);
      } else {
        res.json({ success: true, message: 'OK', cod: 200 });
      }
    }); 
  }
});

// Change a parks availability
router.post('/enabled', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.id || !req.body.enabled) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    if (req.user.role != config.adminRole) {
      res.json({ success: false, message: 'Unauthorized.', cod: 401 });
    } else {
      Park.update(
        { 
          _id: req.body.id 
        },
        {
          'enabled': req.body.enabled 
        }, function(err, park) {
        if (err){
          return next(err);
        } else {
          res.json({ success: true, message: 'OK', cod: 200 });
        }
      }); 
    }
  }
});

// Add a tariff to a park
router.post('/tariff', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.body.id || !req.body.time || !req.body.price) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    var tariff = { time: req.body.time, price: req.body.price };
    Park.update(
      { 
        _id: req.body.id 
      },
      {
        $push: { 'tariff': tariff }
      }, function(err, park) {
      if (err){
         return next(err);
      } else {
        res.json({ success: true, message: 'OK', cod: 200 });
      }
    }); 
  }
});

// Create and add a sensor to a park
router.post('/sensor/:id', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.params.id || !req.body.lat || !req.body.lon) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.findById(req.params.id, function(err, park) {
      if (err){
         return next(err);
      } else {
        var loc =  [req.body.lon, req.body.lat];
        var newSensor = new Sensor({
          type: req.body.type,
          loc: loc,
        });
        newSensor.save(function(err) {
          if (err) {
            return next(err);
          } 
        });
        park._sensors.push(newSensor._id);
        park.save(function(err) {
          if (err){
            return next(err);
          } 
        });
        res.json({ success: true, message: 'OK', cod: 200 });
      }
    }); 
  }
});

// Remove a sensor and it's reference from a park
router.delete('/sensor/:id', passport.authenticate('jwt', { session: false }), function(req, res, next) {
  if(!req.params.id || !req.body.sensorId) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.update(
      { 
        _id: req.params.id 
      },
      { 
        $pull: { '_sensors': req.body.sensorId } 
      }, function(err, park) {
      if (err){
         return next(err);
      } else {
        Sensor.findByIdAndRemove(req.body.sensorId, function(err) {
          if (err) {
            return next(err);
          }
        });
        res.json({ success: true, message: 'OK', cod: 200 });
      }
    }); 
  }
});

// Get a park
router.get('/:id', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.params.id) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.findById(req.params.id, function(err, park) {
      if (err) {
        //return next(err);
        throw err;
      } else {
        res.json({ 
          success: true, 
          message: 'OK', 
          cod: 200,
          item: {
            name: park.name,
            type: park.type,
            size: park.size,
            alerts: park.alerts,
            description: park.description,
            notes: park.notes,
            photo: park.photo,
            geofence: park.geofence,
            loc: park.loc,
            occupation: park.occupation,
            schedule: park.schedule,
            offers: park.offers,
            tariff: park.tariff,
            available: park.available,
            enabled: park.enabled
          }
        });
      } 
    });
  }
});

// Add a new park
router.post('/', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.body.name || !req.body.description || !req.body.size || !req.body.lat || !req.body.lon || !req.body._owner) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    if (req.user.role != config.adminRole) {
      res.json({ success: false, message: 'Unauthorized.', cod: 401 });
    } else {
      var loc =  [req.body.lon, req.body.lat];
      var newPark = new Park({
        name: req.body.name,
        description: req.body.description,
        size: req.body.size,
        photo: req.body.photo,
        loc: loc,
        type: req.body.type,
        geofence: req.body.geofence,
        available: false,
        occupation: 0,
        enabled: false,
        _owner: req.body._owner
      });
      newPark.save(function(err) {
        if (err) {
          throw err;
          //return next(err);
        } else {
          res.json({ success: true, message: 'OK', cod: 200 });
        } 
      });
    }
  }
});

// Update a park
router.patch('/:id', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.params.id) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.findById(req.params.id, function(err, park) {
      if (err) {
        return next(err);
      } else {
        var loc = {
          type: [req.body.lon, req.body.lat],
          index: '2d'     
        };
        park.name = req.body.name,
        park.type = req.body.type,
        park.size = req.body.size,
        park.description = req.body.description,
        park.photo = req.body.photo,
        park.geofence = req.body.geofence,
        park.loc = loc,
        park.available = req.body.available
        park.save(function(err) {
          if (err){
            return next(err);
          } else {
            res.json({ success: true, message: 'OK', cod: 200 });
          }
        });
      } 
    });
  }
});

// Remove a park
router.delete('/', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.body.id) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.findByIdAndRemove(req.body.id, function(err) {
      if (err) {
        return next(err);
      } else {
        res.json({ success: true, message: 'OK', cod: 200 });
      } 
    });
  }
});

// Get all the parks inside a radius
router.post('/search', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.body.lat || !req.body.lon || !req.body.radius) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.find({
       loc: {
        $near: [ req.body.lon , req.body.lat ],
        $maxDistance: req.body.radius/63.710
      }
    }, function(err, parks) {
      if (err) {
        //return next(err);
        throw err;
      } else {
        var outputParks = parks.map(function(obj) {
          return {
            name:obj.name, 
            description:obj.description, 
            size:obj.size, 
            occupation:obj.occupation,
            loc:obj.loc,
            photo:obj.photo,
            schedule:obj.schedule,
            tariff:obj.tariff
          };
        });
        res.json({ success: true, message: 'OK', cod: 200, items: outputParks });
      } 
    });
  }
});

// Get the nearest parks with free parking near a location
router.post('/search/free', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.body.lat || !req.body.lon || !req.body.radius) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.find({
      loc: {
        $near: [ req.body.lon , req.body.lat ],
        $maxDistance: req.body.radius/63.710
      },
      $where:  "this.occupation < this.size"
    }).exec(function(err, parks) {
      if (err) {
        //return next(err);
        throw err;
      } else {
        var outputParks = parks.map(function(obj) {
          return {
            name:obj.name, 
            description:obj.description, 
            size:obj.size, 
            occupation:obj.occupation,
            loc:obj.loc,
            photo:obj.photo,
            schedule:obj.schedule,
            tariff:obj.tariff
          };
        });
        res.json({ success: true, message: 'OK', cod: 200, items: outputParks });
      } 
    });
  }
});

// Flatten a nested array
function steamrollArray(arr) {
  
  var o = [];
  
  for(var j=0;j<arr.length;j++){
    if(Array.isArray(arr[j])){
      subArray(arr[j]);
    } else {
      o.push(arr[j]);
    }
    
  }
  
  function subArray(subarr){
    for(var i=0;i<subarr.length;i++){
      if(Array.isArray(subarr[i])){
        subArray(subarr[i]);
      } else {
        o.push(subarr[i]);
      }
    }
  }
  
  return o;
}

module.exports = router;
