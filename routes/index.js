var express = require('express');
var passport = require('passport');
var cookieParser = require('cookie-parser')
var jwt = require('jsonwebtoken');
var User = require('./../models/user');
var Sensor = require('./../models/sensor');
var SensorLog = require('./../models/sensorLog');
var ParkingLog = require('./../models/parkingLog');
var Park = require('./../models/park');
var config = require('./../config/main');
var moment = require('moment');

var router = express.Router();

router.use(cookieParser())

/* GET home page. */
router.get('/', 
/*passport.authenticate('jwt', { 
    session: false, 
    successRedirect: '/', 
    failureRedirect: '/login' 
  }),*/
  function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

    if (token) {
      res.render('index', { title: 'Parksense Admin Panel' });
    } else {
      res.render('login', { title: 'Parksense Admin Panel' });
    }
    
});

/* GET login page. */
router.get('/login', function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

    if (token) {
      res.render('login', { title: 'Parksense Admin Panel' });
    } else {
      res.render('index', { title: 'Parksense Admin Panel' });
    }
  
});

router.get('/users', function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

    if (token) {
      User.find({}, function(err, users) {
        if (err) throw err;
        if (users) {
          var output = users.map(function(obj) {
            return { 
              role: obj.role, 
              email: obj.email, 
              createdAt: moment.utc(obj.createdAt).format("DD-MM-YYYY HH:mm"),
              _id: obj._id
            }
          });
          res.render('users', { title: 'Parksense Admin Panel', list: JSON.stringify(output), listTitle: 'Users' });
        } else {
          res.render('users', { title: 'Parksense Admin Panel', list: [], listTitle: 'Users'  });
        }
      });
    } else {
      res.render('login', { title: 'Parksense Admin Panel' });
    }
});

router.get('/parks', function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

    if (token) {
      Park.find({}, function(err, parks) {
        if (err) throw err;
        if (parks) {
          var output = parks.map(function(obj) {
            return { 
              name: obj.name, 
              type: obj.type, 
              size: obj.size, 
              loc: obj.loc, 
              occupation: obj.occupation, 
              schedule: obj.schedule, 
              available: obj.available, 
              enabled: obj.enabled, 
              createdAt: moment.utc(obj.createdAt).format("DD-MM-YYYY HH:mm"),
              _id: obj._id
            }
          });
          res.render('parks', { title: 'Parksense Admin Panel', list: JSON.stringify(output), listTitle: 'Parks' });
        } else {
          res.render('parks', { title: 'Parksense Admin Panel', list: [], listTitle: 'Parks'  });
        }
      });
    } else {
      res.render('login', { title: 'Parksense Admin Panel' });
    }
});

router.get('/parkinglogs', function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

    if (token) {
      ParkingLog.find({}, function(err, parkinglogs) {
        if (err) throw err;
        if (parkinglogs) {
          var output = parkinglogs.map(function(obj) {
            return { 
              loc: obj.loc,  
              _park: obj._park, 
              date: moment.utc(obj.date).format("DD-MM-YYYY HH:mm"),
              _id: obj._id
            }
          });
          res.render('parkinglogs', { title: 'Parksense Admin Panel', list: JSON.stringify(output), listTitle: 'Parking Logs' });
        } else {
          res.render('parkinglogs', { title: 'Parksense Admin Panel', list: [], listTitle: 'Parking Logs'  });
        }
      });
    } else {
      res.render('login', { title: 'Parksense Admin Panel' });
    }
});

router.get('/sensors', function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

    if (token) {
      Sensor.find({}, function(err, sensors) {
        if (err) throw err;
        if (sensors) {
          var output = sensors.map(function(obj) {
            return { 
              loc: obj.loc,  
              notes: obj.notes, 
              type: obj.type, 
              createdAt: moment.utc(obj.createdAt).format("DD-MM-YYYY HH:mm"),
              _id: obj._id
            }
          });
          res.render('sensors', { title: 'Parksense Admin Panel', list: JSON.stringify(output), listTitle: 'Sensors' });
        } else {
          res.render('sensors', { title: 'Parksense Admin Panel', list: [], listTitle: 'Sensors'  });
        }
      });
    } else {
      res.render('login', { title: 'Parksense Admin Panel' });
    }
});

router.get('/sensorlogs', function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

    if (token) {
      SensorLog.find({}, function(err, sensorlogs) {
        if (err) throw err;
        if (sensorlogs) {
          var output = sensorlogs.map(function(obj) {
            return { 
              loc: obj.loc,  
              count: obj.count, 
              date: moment.utc(obj.date).format("DD-MM-YYYY HH:mm"),
              _id: obj._id
            }
          });
          res.render('sensorlogs', { title: 'Parksense Admin Panel', list: JSON.stringify(output), listTitle: 'Sensor Logs' });
        } else {
          res.render('sensorlogs', { title: 'Parksense Admin Panel', list: [], listTitle: 'Sensor Logs'  });
        }
      });
    } else {
      res.render('login', { title: 'Parksense Admin Panel' });
    }
});

router.get('/registerparkadmin', function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

    if (token) {
      res.render('addParkAdmin', { title: 'Parksense Admin Panel' });
    } else {
      res.render('login', { title: 'Parksense Admin Panel' });
    }
    
});

router.get('/registerpark', function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

    if (token) {
      User.find({ role: 'Manager' }, function(err, managers) {
        if (err) throw err;
        if (managers) {
          var output = managers.map(function(obj) {
            return { 
              email: obj.email,
              createdAt: moment.utc(obj.createdAt).format("DD-MM-YYYY HH:mm"),
              _id: obj._id
            }
          });
          res.render('addPark', { title: 'Parksense Admin Panel', managers: JSON.stringify(output) });
        } else {
          res.render('addPark', { title: 'Parksense Admin Panel', managers: [] });
        }
      });
    } else {
      res.render('login', { title: 'Parksense Admin Panel' });
    }
});

router.get('/park', function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;
    if (token) {
      if (req.query.id) {
        Park.findById(req.query.id, function(err, park) {
          if (err) throw err;
          if (park) {
            res.render('parkDetails', { title: 'Parksense Admin Panel', park: JSON.stringify(park) });
          } else {
            res.render('parks', { title: 'Parksense Admin Panel', list: [], listTitle: 'Parks' });
          }
        });
      } else {
        res.render('index', { title: 'Parksense Admin Panel'});
      }
    } else {
      res.render('login', { title: 'Parksense Admin Panel' });
    }
});

module.exports = router;
