var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var Sensor = require('./../models/sensor');
var User = require('./../models/user');
var Park = require('./../models/park');

// Get all parks belonging to a user
router.get('/parks/:id', passport.authenticate('jwt', { session: false }), function(req, res) {
  if(!req.params.id) {
    res.json({ success: false, message: 'Invalid Parameters', cod: 400 });
  } else {
    Park.find({ 
      '_owner': mongoose.Types.ObjectId(req.params.id)
    }, function(err, parks) {
      if (err) {
        //return next(err);
        throw err;
      } else {
        var outputParks = parks.map(function(obj) {
          return {
            name: obj.name,
            type: obj.type,
            size: obj.size,
            alerts: obj.alerts,
            description: obj.description,
            notes: obj.notes,
            photo: obj.photo,
            geofence: obj.geofence,
            loc: obj.loc,
            occupation: obj.occupation,
            schedule: obj.schedule,
            offers: obj.offers,
            tariff: obj.tariff,
            available: obj.available,
            enabled: obj.enabled
          }
        });
        res.json({ success: true, message: 'OK', cod: 200, items: outputParks });
      } 
    });
  }
});

module.exports = router;
