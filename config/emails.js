var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
var config = require('./../config/main');
var client = nodemailer.createTransport(sgTransport(config.nodemailer));

module.exports = {
  
  registerEmail: function (email, pw) {
    var emailCompose = {
      from: '"Parksense" <notice@parksense.com>',
      to: email,
      subject: 'Registration 🚘',
      //text: 'Hello world 🚘',
      html: '<h3>Welcome to Parksense</h3>\
            <p><b>Thank you for your registration!</b></p>\
            <p>Email: ' + email + '</p>\
            <p>Password: ' + pw + '</p>'
    };
    
    client.sendMail(emailCompose, function(err, info){
      if (err ){
        console.log(error);
      }
      else {
        console.log('Message sent');
      }
    });
  },
  
  passwordResetEmail: function (email, token) {
    var emailCompose = {
      from: '"Parksense" <notice@parksense.com>',
      to: email,
      subject: 'Password Reset 🚘',
      //text: 'Hello world 🚘',
      html: '<h3>Reset your password</h3>\
            <p><b>Click the link below</b></p>\
            <p><a href="#"> .../reset/' + token + ' </a></p>'
    };
    
    client.sendMail(emailCompose, function(err, info){
      if (err){
        console.log(error);
      }
      else {
        console.log('Message sent');
      }
    });
  },
  
};
