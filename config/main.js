module.exports = {
    // login secret
    'secret': 'putsomethingtopsecrethere',
    // new pw request secret
    'resetsecret': 'putsomethingtopsecretherereset',
    // db connection string
    'database': 'mongodb://admin:admin123@ds015953.mlab.com:15953/psense',
    // email server pw
    'nodemailer': {
        auth: {
            api_user: 'psense',
            api_key: 'aBc123!#'
        }
    },
    // regular token expire time, 1h default
    'tokenexpire' : 3600000,
    // Admin role for accessing restricted api routes
    'adminRole' : 'Admin'
};