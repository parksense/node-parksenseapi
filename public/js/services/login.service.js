angular
  .module('parksense')
  .factory('LoginService', ['$http', function($http) {

    return {
      login : function(email, password) {
        return $http.post('/api/v1/users/authenticate', {
          email: email,
          password: password
        });
      }
      
    }     
}]);