angular
  .module('parksense')
  .directive('addParkAdminForm', ['$http','$cookies', function($http,$cookies){
    return {
      restrict: 'E',
      templateUrl: 'js/directives/add-park-admin-form/add-park-admin-form.html',
      link: function (scope, element, attrs) {
        scope.buttonText = 'Submit';
        
        scope.submitForm = function() {
          scope.buttonText = 'Sending...';
          $http({
            method: 'POST',
            url: '/api/v1/users/registerAdmin',
            headers: {
              'Authorization': $cookies.get('token')
            },
            data: {
              email: scope.user.email,
              password: scope.user.password,
              role: 'Manager'
            }
          }).then(function successCallback(response) {
            if (!response.data.success) {
              scope.buttonText = 'Error';
            } else {
              scope.buttonText = 'Success';
            }
          }, function errorCallback(response) {
            scope.buttonText = 'Error';
          });
        }
      }
    };
}]);
