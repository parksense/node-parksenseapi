angular
  .module('parksense')
  .directive('sensorList', sensorList);

  function sensorList() {

    // Definition of directive
    var directiveDefinitionObject = {
      restrict: 'E',
      templateUrl: 'js/directives/sensor-list/sensor-list.html',
      link: function (scope, element, attrs) {
        scope.listTitle = attrs.listtitle;
        scope.list = JSON.parse(attrs.list);
        console.log(scope.list)
      }
    };

    return directiveDefinitionObject;
  }