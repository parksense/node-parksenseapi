angular
  .module('parksense')
  .directive('parkList', parkList);

  function parkList() {

    // Definition of directive
    var directiveDefinitionObject = {
      restrict: 'E',
      templateUrl: 'js/directives/park-list/park-list.html',
      link: function (scope, element, attrs) {
        scope.listTitle = attrs.listtitle;
        scope.list = JSON.parse(attrs.list);
        
        scope.navigateToPark = function(id) {
          location.assign('/park?id=' + id);
        }
      }
    };

    return directiveDefinitionObject;
  }