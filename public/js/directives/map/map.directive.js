angular
  .module('parksense')
  .directive('map', ['$http','$cookies', function($http,$cookies){
    return {
      restrict: 'E',
      templateUrl: 'js/directives/map/map.html',
      link: function (scope, element, attrs) {
        var map = L.map('mapid').setView([51.505, -0.09], 13);
        var plotlist;
        var plotlayers=[];

        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a>',
          maxZoom: 18
        }).addTo(map);
        
        map.setView(new L.LatLng(38.521593, -8.839177),12);
        
        askForPlots();
	      map.on('moveend', onMapMove);
        
        function onMapMove(e) { askForPlots(); }
        
        function removeMarkers() {
          for (i = 0; i < plotlayers.length; i++) {
            map.removeLayer(plotlayers[i]);
          }
          plotlayers=[];
        }
        
        function stateChanged(plotlist) {
          removeMarkers();
          
          for (i = 0; i < plotlist.length; i++) {
            var plotll = new L.LatLng(plotlist[i].loc[1],plotlist[i].loc[0], true);
            var plotmark = new L.Marker(plotll);
            plotmark.data=plotlist[i];
            map.addLayer(plotmark);
            plotmark.bindPopup("<h3>"+plotlist[i].name+"</h3> Occupation: "+plotlist[i].occupation+'/'+plotlist[i].size);
            plotlayers.push(plotmark);
            
          }
        }
        
        function askForPlots() {
          var bounds=map.getBounds();
          var minll=bounds.getSouthWest();
          var maxll=bounds.getNorthEast();
          $http({
            method: 'POST',
            url: '/api/v1/park/parksbox',
            headers: {
              'Authorization': $cookies.get('token')
            },
            data: {
              bllon: minll.lng,
              bllat: minll.lat,
              urlon: maxll.lng,
              urlat: maxll.lat
            }
          }).then(function successCallback(response) {
            if (!response.data.success) {
              console.log(response)
            } else {
              stateChanged(response.data.items)
            }
          }, function errorCallback(response) {
            console.log(response)
          });
        }
        
      }
    };
}]);