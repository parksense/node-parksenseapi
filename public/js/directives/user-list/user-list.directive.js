angular
  .module('parksense')
  .directive('userList', userList);

  function userList() {

    // Definition of directive
    var directiveDefinitionObject = {
      restrict: 'E',
      templateUrl: 'js/directives/user-list/user-list.html',
      link: function (scope, element, attrs) {
        scope.listTitle = attrs.listtitle;
        scope.list = JSON.parse(attrs.list);
      }
    };

    return directiveDefinitionObject;
  }