angular
  .module('parksense')
  .directive('sensorlogsList', sensorlogsList);

  function sensorlogsList() {

    // Definition of directive
    var directiveDefinitionObject = {
      restrict: 'E',
      templateUrl: 'js/directives/sensorlogs-list/sensorlogs-list.html',
      link: function (scope, element, attrs) {
        console.log(JSON.parse(attrs.list))
        scope.listTitle = attrs.listtitle;
        scope.list = JSON.parse(attrs.list);
      }
    };

    return directiveDefinitionObject;
  }