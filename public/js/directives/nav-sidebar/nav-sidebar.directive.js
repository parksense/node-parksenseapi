angular
  .module('parksense')
  .directive('navSidebar', navSidebar);

  function navSidebar() {

    // Definition of directive
    var directiveDefinitionObject = {
      restrict: 'E',
      templateUrl: 'js/directives/nav-sidebar/nav-sidebar.html',
      link: function (scope) {
        scope.pages = [
          { name: 'Home', link: '/' },
          { name: 'Users', link: '/users' },
          { name: 'Parks', link: '/parks' },
          { name: 'Parking Logs', link: '/parkinglogs' },
          { name: 'Sensors', link: '/sensors' },
          { name: 'Sensor Logs', link: '/sensorlogs' }
        ];
        
        scope.navigateTo = function(page) {
          location.assign(page);
        }
      }
    };

    return directiveDefinitionObject;
  }