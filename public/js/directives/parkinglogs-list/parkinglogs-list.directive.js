angular
  .module('parksense')
  .directive('parkinglogsList', parkinglogsList);

  function parkinglogsList() {

    // Definition of directive
    var directiveDefinitionObject = {
      restrict: 'E',
      templateUrl: 'js/directives/parkinglogs-list/parkinglogs-list.html',
      link: function (scope, element, attrs) {
        scope.listTitle = attrs.listtitle;
        scope.list = JSON.parse(attrs.list);
      }
    };

    return directiveDefinitionObject;
  }