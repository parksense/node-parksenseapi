angular
  .module('parksense')
  .directive('addParkForm', ['$http','$cookies', function($http,$cookies){
    return {
      restrict: 'E',
      templateUrl: 'js/directives/add-park-form/add-park-form.html',
      link: function (scope, element, attrs) {
        scope.managers = JSON.parse(attrs.managers);
        console.log(scope.managers)
        
        scope.buttonText = 'Submit';
        
        scope.submitForm = function() {
          scope.buttonText = 'Sending...';
          console.log(scope.park)
          $http({
            method: 'POST',
            url: '/api/v1/park',
            headers: {
              'Authorization': $cookies.get('token')
            },
            data: {
              name: scope.park.name,
              description: scope.park.description,
              _owner: scope.park._owner,
              size: scope.park.size,
              geofence: scope.park.geofence,
              type: scope.park.type,
              lat: scope.park.lat,
              lon: scope.park.lon
            }
          }).then(function successCallback(response) {
            if (!response.data.success) {
              scope.buttonText = 'Error';
            } else {
              scope.buttonText = 'Success';
            }
          }, function errorCallback(response) {
            scope.buttonText = 'Error';
          });
        }
      }
    };
}]);
