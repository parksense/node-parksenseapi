angular
  .module('parksense')
  .directive('parkDetails', ['$http','$cookies','$timeout', function($http,$cookies,$timeout){
    return {
      restrict: 'E',
      templateUrl: 'js/directives/park-details/park-details.html',
      link: function (scope, element, attrs) {
        scope.park = JSON.parse(attrs.park);
        scope.sensorForm = false;
        scope.sensorButton = "New Sensor";
        scope.sensorIcon = "add";
        scope.sensorSubmitButton = "Submit";
        scope.sensors = [];
        scope.logs = [];
        
        console.log(scope.park)
        
        // Change park availability
        scope.updateAvailability = function() {
          $http({
            method: 'POST',
            url: '/api/v1/park/enabled',
            headers: {
              'Authorization': $cookies.get('token')
            },
            data: {
              id: scope.park._id,
              enabled: scope.park.enabled
            }
          }).then(function successCallback(response) {
            if (!response.data.success) {
              console.log(response)
            } else {
              console.log(response)
            }
          }, function errorCallback(response) {
            console.log(response)
          });
        }
        
        // Get logs
        scope.getLogs = function() {
          $http({
            method: 'POST',
            url: '/api/v1/park/logs/latest',
            headers: {
              'Authorization': $cookies.get('token')
            },
            data: {
              id: scope.park._id
            }
          }).then(function successCallback(response) {
            if (!response.data.success) {
              console.log(response)
            } else {
              console.log(response)
              scope.logs = response.data.items;
            }
          }, function errorCallback(response) {
            console.log(response)
          });
        }
        
        // Get sensores
        scope.getSensores = function() {
          $http({
            method: 'GET',
            url: '/api/v1/park/sensors/all/'+scope.park._id,
            headers: {
              'Authorization': $cookies.get('token')
            }
          }).then(function successCallback(response) {
            if (!response.data.success) {
              console.log(response)
            } else {
              console.log(response)
              scope.sensors = response.data.items;
            }
          }, function errorCallback(response) {
            console.log(response)
          });
        }
        
        // Submit sensor
        scope.submitSensor = function() {
          scope.sensorSubmitButton = 'Sending...';
          $http({
            method: 'POST',
            url: '/api/v1/park/sensor/'+scope.park._id,
            headers: {
              'Authorization': $cookies.get('token')
            },
            data: {
              lat: scope.sensor.lat,
              lon: scope.sensor.lon
            }
          }).then(function successCallback(response) {
            if (!response.data.success) {
              scope.sensorSubmitButton = 'Error';
            } else {
              scope.sensorSubmitButton = 'Success';
              scope.getSensores();
            }
          }, function errorCallback(response) {
            scope.sensorSubmitButton = 'Error';
          });
        }
        
        // New sensor show hide button
        scope.sensorFormShow = function() {
          if (!scope.sensorForm) {
            scope.sensorForm = true;
            scope.sensorButton = "Close";
            scope.sensorIcon = "close";
          } else {
            scope.sensorForm = false;
            scope.sensorButton = "New Sensor";
            scope.sensorIcon = "add";
          }
        }
        
      }
    };
}]);




