angular
  .module('parksense')
  .controller('LoginController', ['LoginService','$localStorage','$location', function(LoginService,$localStorage,$location) {

    var self = this;
    self.loginError = false;
    
    function handleRequest(res) {
      var token = res.data ? res.data.token : null;
      if(token) { 
        self.loginError = false;
        $localStorage.JWT = token
      } else {
        self.loginError = true;
      }
    }
    
    self.login = function() {
      LoginService.login(self.email, self.password)
        .then(handleRequest, handleRequest)
    }
  }]);